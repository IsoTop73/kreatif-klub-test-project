<?php

namespace App\Http\Controllers;

use App\Http\Resources\Item AS ItemResource;
use App\Models\Item;
use App\Models\ItemView;

class ItemsController extends Controller
{
    public function index()
    {
        return ItemResource::collection(
            auth()->user()->items
        );
    }

    public function view(int $id)
    {
        /* @var Item $item */
        $item = auth()->user()->items()->find($id);

        if (!$item) {
            abort(403, 'Forbidden');
        }

        ItemView::create([
            'item_id' => $id,
            'user_id' => auth()->id(),
            'viewed_at' => now()
        ]);

        $item->load('itemViews');

        return new ItemResource($item);
    }
}
