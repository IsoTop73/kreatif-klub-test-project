<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Faker\Generator AS Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param Faker $faker
     * @return void
     */
    public function run(Faker $faker)
    {
        $data = [
            [
                'name' => $faker->name,
                'email' => 'employee@mail.com',
                'password' => Hash::make('password'),
                'role_id' => 1,
            ],
            [
                'name' => $faker->name,
                'email' => 'manager@mail.com',
                'password' => Hash::make('password'),
                'role_id' => 2,
            ],
            [
                'name' => $faker->name,
                'email' => 'director@mail.com',
                'password' => Hash::make('password'),
                'role_id' => 3,
            ]
        ];

        \App\Models\User::insert($data);
    }
}
