import Vue from 'vue';
import store from './store';
import router from './router';
import App from './components/App';
import vuetify from './plugins/vuetify';

Vue.prototype.axios = require('axios');
Vue.prototype.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

if (window.user) {
    store.dispatch('login', window.user);
}

new Vue({
    vuetify,
    router,
    store,
    render: h => h(App)
}).$mount('#app');
