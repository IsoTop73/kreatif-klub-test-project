<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemView extends Model
{
    protected $table = 'items_views';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'item_id',
        'viewed_at'
    ];
}
