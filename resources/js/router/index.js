import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store';

const routes = [
    {
        path: '/',
        component: require('../components/pages/main/Index').default,
        meta: {
            requiresAuth: true
        },
        children: [
            {
                path: 'items',
                component: require('../components/pages/items/Index').default
            },
            {
                path: 'item/:id',
                component: require('../components/pages/item/Index').default
            }
        ]
    },
    {
        path: '/login',
        name: 'login',
        component: require('../components/pages/login/Index').default
    },
    {
        path: '*',
        component: require('../components/pages/NotFound').default
    }
];

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {
    if (to.name === 'login' && store.getters.isLoggedIn) {
        return next({
            path: '/'
        });
    }

    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.getters.isLoggedIn) {
            next({
                name: 'login'
            });
        } else {
            next();
        }
    } else {
        next();
    }
});

export default router;
