<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth'], function () {
    Route::get('item/{id}', 'ItemsController@view');
    Route::get('items', 'ItemsController@index');
});

Route::post('login', 'Auth\\LoginController@login')->name('login');
Route::get('logout', 'Auth\\LoginController@logout');

Route::fallback(function () {
    return view('index');
});
