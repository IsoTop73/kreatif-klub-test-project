<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleItem extends Model
{
    protected $table = 'roles_items';

    public $timestamps = false;

    protected $fillable = [
        'role_id',
        'item_id'
    ];
}
