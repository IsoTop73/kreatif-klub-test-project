<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'employee'
            ],
            [
                'name' => 'manager'
            ],
            [
                'name' => 'director'
            ]
        ];

        \App\Models\Role::insert($data);
    }
}
