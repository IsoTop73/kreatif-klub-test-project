<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';

    protected $fillable = [
        'title',
        'description',
        'image_url'
    ];

    public function itemViews()
    {
        return $this->hasMany(ItemView::class);
    }
}
