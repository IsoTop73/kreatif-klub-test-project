<?php

use Illuminate\Database\Seeder;

class RoleItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $rolesId = \App\Models\Role::query()->pluck('id');
        $itemsId = \App\Models\Item::query()->pluck('id');

        foreach ($rolesId as $roleId) {
            $_itemsId = $itemsId;

            if ($roleId === 1) {
                $_itemsId = $itemsId->shuffle()->take(3);
            }

            foreach ($_itemsId as $itemId) {
                $data[] = [
                    'role_id' => $roleId,
                    'item_id' => $itemId
                ];
            }
        }

        \App\Models\RoleItem::insert($data);
    }
}
