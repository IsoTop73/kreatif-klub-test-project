<?php

namespace App\Http\Middleware;

use Closure;

class SPA
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->ajax() && $request->isMethod('GET')) {
            return response()->view('index');
        }

        return $next($request);
    }
}
