<?php

use Illuminate\Database\Seeder;
use Faker\Generator AS Faker;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param Faker $faker
     * @return void
     */
    public function run(Faker $faker)
    {
        $data = [];
        $urls = [
            'https://luxfon.com/pic/201203/1920x1080/luxfon.com-464.jpg',
            'http://luxfon.com/pic/201203/1920x1080/luxfon.com-714.jpg',
            'http://luxfon.com/pic/201203/1920x1080/luxfon.com-1857.jpg',
            'http://luxfon.com/pic/201203/1920x1080/luxfon.com-3023.jpg',
            'http://luxfon.com/pic/201203/1920x1080/luxfon.com-324.jpg',
            'http://luxfon.com/pic/201203/1920x1080/luxfon.com-2817.jpg'
        ];

        foreach ($urls as $url) {
            $data[] = [
                'title' => $faker->words(3, true),
                'description' => $faker->text,
                'image_url' => $url,
            ];
        }

        \App\Models\Item::insert($data);
    }
}
