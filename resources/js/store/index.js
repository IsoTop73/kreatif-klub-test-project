import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: null
    },
    mutations: {
        login(state, payload) {
            state.user = payload;
        },
        logout(state) {
            state.user = null;
        }
    },
    actions: {
        login(context, payload) {
            context.commit('login', payload);
        },
        logout(context) {
            context.commit('logout');
        }
    },
    getters: {
        isLoggedIn(state) {
            return state.user !== null;
        }
    }
});
